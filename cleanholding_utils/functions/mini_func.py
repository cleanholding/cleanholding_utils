def dict_has_field(dict, field):
    if not field in dict:
        return False
    obj = dict[field]
    if obj is None:
        return False
    if isinstance(obj, str):
        if obj == '':
            return False
    return True
