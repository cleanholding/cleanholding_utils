"""
An sms.ru client.
from smsru import Client
Client(SENDER, KEY).send("+79112223344", "привет лунатикам")
"""

import requests

SEND_STATUS = {
    -1: 'Message not found',
    100: 'Message is in the queue',
    101: 'Message is on the way to the operator',
    102: 'Message is on the way to the recipient',
    103: 'Message delivered',
    104: 'Message failed: out of time',
    105: 'Message failed: cancelled by the operator',
    106: 'Message failed: phone malfunction',
    107: 'Message failed, reason unknown',
    108: 'Message declined',
    150: 'Cannot be delivered, unable to find the route for the phone',
    201: 'Out of money',
    202: 'Incorrect phone number',
    203: 'Empty message',
    204: 'Bad sender (unapproved)',
    205: 'Message too long',
    206: 'Day message limit reached',
    207: 'Can\'t send messages to that number',
    208: 'Wrong time',
    209: 'Blacklisted recipient',
    212: 'Incorrect message encoding',
    215: 'Recipient added sender as a spammer',
    231: 'Reached limit of identical messages on the same phone per minute',
    232: 'Reached limit of identical messages on the same phone per day',
    500: 'Internal service error',
}

COST_STATUS = {
    100: 'Success'
}

BLACK_WORD_LIST = [
    "россыпь",
    "микс",
    "rossyp'",
    "miks",
]


class SMSRuException(Exception):
    pass


class Client(object):
    def __init__(self, SENDER, KEY):
        self.sender = SENDER
        self.key = KEY
        self.session = requests.Session()

    def _call(self, method, args):
        """Calls a remote method."""
        if not isinstance(args, dict):
            raise ValueError('args must be a dictionary')
        args['api_id'] = self.key
        args['json'] = 1
        try:
            res = self.session.get('https://sms.ru/%s' % method, params=args, timeout=10)
            res = res.json()
        except requests.exceptions.JSONDecodeError as e:
            raise SMSRuException(str(e))
        except requests.exceptions.RequestException as e:
            raise SMSRuException(f"Error while sending request to sms.ru: {e}")
        status_code = res['status_code']
        if status_code < 200:
            return res
        raise SMSRuException(SEND_STATUS.get(status_code, status_code))

    def send(self, to, message, express=False, test=False):
        """Sends the message to the specified recipient.  Returns a numeric
        status code, its text description and, if the message was successfully
        accepted, its reference number."""
        for bw in BLACK_WORD_LIST:
            message = message.replace(' ' + bw, '')
            message = message.replace(bw + ' ', '')
        args = {'to': to, 'text': message.encode('utf-8'), 'from': self.sender}
        if express:
            args['express'] = '1'
        if test:
            args['test'] = '1'
        return self._call('sms/send', args)

    def status(self, msgid):
        """Returns message status."""
        STATUS_FOUND = 100
        res = self._call('sms/status', {'id': msgid})
        status_code = res['status_code']
        if status_code != STATUS_FOUND:
            status = 100
        else:
            status = res['sms'][msgid]['status_code']
        text = SEND_STATUS.get(status, 'Unknown status')
        return status, text

    def cost(self, to, message):
        """Prints the cost of the message."""
        res = self._call('sms/cost', {'to': to, 'text': message})
        return res['total_cost']

    def balance(self):
        """Returns your current balance."""
        res = self._call('my/balance', {})
        if res['status_code'] == 100:
            return res['balance']
        raise Exception(res)

    def limit(self):
        """Returns the remaining message limit."""
        res = self._call('my/limit', {})
        if res['status_code'] == 100:
            return res
        raise Exception(res[0])
