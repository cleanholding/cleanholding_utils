from pgtrigger import Trigger


TRIGGER_FUNC_SCHEMA = {
    'type': 'object',
    'properties': {
        'function_name': {'type': 'string'},
        'relation': {'type': 'string'},
        'args': {
            'type': 'array',
            'items': {'type': 'string'}
        },
        'related_attributes': {
            'type': 'array',
            'items': {'type': 'string'}}
    },
    'additionalItems': False
}


class TrackFieldTrigger(Trigger):

    def func_template(self, alias):
        fields = [alias + '.' + field_name for field_name in self.func.get('args')]
        return f"PERFORM {self.func.get('function_name')}({', '.join(fields)});"

    def get_func(self, model):
        """
        Generate the trigger function that comes between the BEGIN and END
        clause with given args
        """
        try:
            import jsonschema
            jsonschema.validate(instance=self.func, schema=TRIGGER_FUNC_SCHEMA)
        except ImportError:
            pass
        operations_actions = {
            'DELETE': f"IF (TG_OP = 'DELETE') THEN {self.func_template('OLD')} ",
            'INSERT': f"ELSIF (TG_OP = 'INSERT') THEN {self.func_template('NEW')} ",
            'UPDATE': "ELSIF (TG_OP = 'UPDATE') THEN ",
        }
        trigger = ''
        operations_list = sorted(str(self.operation).split(' OR '))
        if len(operations_list) > 1:
            for operation in operations_list:
                trigger += operations_actions.get(operation)
        else:
            trigger += operations_actions.get(operations_list[0]).replace('ELSIF', 'IF')

        if 'UPDATE' in operations_list:
            condition = []
            relation_field = self.func.get('relation')
            related_attributes = self.func.get('related_attributes')
            if relation_field:
                related_attributes.append(relation_field)
            for field in related_attributes:
                condition.append(f"NEW.{field} IS DISTINCT FROM OLD.{field} ")
            trigger += f"IF({' OR '.join(condition)}) THEN {self.func_template('NEW')} "
            if relation_field:
                trigger += f"""
                IF (NEW.{relation_field} IS DISTINCT FROM OLD.{relation_field}) THEN
                    {self.func_template('OLD')}
                END IF;
                """
            trigger += "END IF;"
        trigger += "END IF; RETURN NULL;"
        return trigger
