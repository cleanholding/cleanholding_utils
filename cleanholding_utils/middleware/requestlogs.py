import json
import logging
import time
from copy import deepcopy
from urllib.parse import parse_qs, urlencode, urlparse, urlunparse

import environ
from django.contrib.admindocs.views import simplify_regex
from django.http import StreamingHttpResponse
from django.urls import resolve

ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('ubirator')

env = environ.Env()
logger = logging.getLogger(__name__)


class RequestLoggingMiddleware:

    def __init__(self, get_response):
        self.get_response = get_response

    def filtered_path(self, path):
        if 'telegrambot' in path:
            return '/api/telegrambot/' + path.split('/')[-1]
        filter_words = ['token', 'secret']
        u = urlparse(path)
        query = parse_qs(u.query, keep_blank_values=True)
        for w in filter_words:
            query.pop(w, 'filtered')
        u = u._replace(query=urlencode(query, True))
        return urlunparse(u)

    def request_data(self, request):
        filter_words = ['token', 'secret', 'password', 'auth']

        request_length = len(request.body)
        data = {
            'content_type': request.content_type,
            'size': request_length,
        }

        if user_agent := request.META.get('HTTP_USER_AGENT', ''):
            data['user_agent'] = user_agent

        if request_length >= 5000:
            data['body_hidden_reason'] = 'too large'
            return data

        if ('application/json' in request.META.get('CONTENT_TYPE', '')
                and request_length > 0):
            try:
                body = json.loads(request.body)
                if type(body) == dict:
                    for w in filter_words:
                        if w in data:
                            body[w] = 'filtered'
                    data['body'] = body
                elif type(body) == list:
                    data['body'] = body
                else:
                    data['body_hidden_reason'] = 'unknown format'
            except json.decoder.JSONDecodeError:
                data['body_hidden_reason'] = 'json cannot be parsed'
        else:
            data['body_hidden_reason'] = 'json cannot be parsed'
        return data

    def response_data(self, response):
        filter_words = ['token', 'secret', 'password', 'auth']
        if isinstance(response, StreamingHttpResponse):
            response_length = int(response.get('Content-Length', 1))
        else:
            response_length = len(response.content)
        data = {
            'size': response_length,
        }

        if response_length >= 5000 or not isinstance(data, dict):
            data['body_hidden_reason'] = 'too large'
            return data

        body = deepcopy(getattr(response, 'data', {})) or {}
        if type(body) == dict:
            for w in filter_words:
                if w in body:
                    body[w] = 'filtered'
            data['body'] = body
        elif type(body) == list:
            data['body'] = body
        else:
            data['body_hidden_reason'] = 'unknown format'

        return data

    def get_clean_path(self, request):
        path = request.path_info
        resolve_url_data = resolve(path)
        return request.method + simplify_regex(resolve_url_data.route)

    def __call__(self, request):
        _t = time.time()
        request_data = self.request_data(request)
        response = self.get_response(request)
        _t = (time.time() - _t)

        log_data = {
            'user': {
                **self.get_client_ip_data(request),
                'id': str(getattr(request.user, 'id', 'anonymous')),
            },
            'path': self.filtered_path(request.get_full_path()),
            'clean_path': self.get_clean_path(request),
            'method': request.method,
            'request': request_data,
        }


        log_data['type'] = 'response'
        log_data['response'] = self.response_data(response)
        log_data['status_code'] = response.status_code
        log_data['execution_time'] = _t
        logger.info('request data', extra=log_data)
        return response

    # get clients ip address
    def get_client_ip_data(self, request):
        data = {}
        x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
        x_real_ip = request.META.get('HTTP_X_REAL_IP')
        if x_forwarded_for:
            data["ip_forwarded"] = x_forwarded_for.split(',')[0]
        if x_real_ip:
            data["ip"] = x_real_ip
        return data
