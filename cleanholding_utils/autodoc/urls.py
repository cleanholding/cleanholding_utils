from django.urls import path

from . import views


urlpatterns = [
    path('', views.AutoDocView.as_view(), name='autodoc'),
]
