"""
This source is taken from
https://github.com/Hipo/drf-extra-fields
"""
import json

from django.contrib.gis.geos import GEOSGeometry, Polygon
from django.contrib.gis.geos.error import GEOSException
from django.utils.encoding import smart_str
from rest_framework import serializers

EMPTY_VALUES = (None, '', [], (), {})


class PointField(serializers.Field):
    """
    A field for handling GeoDjango Point fields as na json format.
    Expected input format:
        {
         "latitude": 49.8782482189424,
         "longitude": 24.452545489
        }
    """
    type_name = 'PointField'
    type_label = 'point'

    default_error_messages = {
        'invalid': 'Enter a valid location.',
    }

    def __init__(self, *args, **kwargs):
        self.str_points = kwargs.pop('str_points', False)
        super(PointField, self).__init__(*args, **kwargs)

    def to_internal_value(self, value):
        """
        Parse json data and return a point object
        """
        if value in EMPTY_VALUES and not self.required:
            return None

        if isinstance(value, str):
            try:
                value = value.replace("'", '"')
                value = json.loads(value)
            except ValueError:
                self.fail('invalid')

        if value and isinstance(value, dict):
            try:
                latitude = value.get('latitude')
                longitude = value.get('longitude')
                return GEOSGeometry('POINT(%(longitude)s %(latitude)s)' % {
                    'longitude': longitude,
                    'latitude': latitude}
                                    )
            except (GEOSException, ValueError):
                self.fail('invalid')
        self.fail('invalid')

    def to_representation(self, value):
        """
        Transform POINT object to json.
        """
        if value is None:
            return value

        if isinstance(value, GEOSGeometry):
            value = {
                'latitude': value.y,
                'longitude': value.x
            }

        if self.str_points:
            value['longitude'] = smart_str(value.pop('longitude'))
            value['latitude'] = smart_str(value.pop('latitude'))

        return value


# Part not copied from elsewhere
class PolygonField(serializers.Field):
    # [[x, y], ...] or [[longitude, latitude], ...]
    # Example value = [[0.0, 0.0], [0.0, 50.0], [50.0,50.0], [50.0, 0.0], [0.0, 0.0]]
    type_name = 'PolygonField'
    type_label = 'polygon'

    default_error_messages = {
        'invalid': 'Enter a valid polygon.'
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def to_internal_value(self, value):
        """
        Parse json data and return a polygon object
        """
        if value in EMPTY_VALUES and not self.required:
            return None

        if isinstance(value, str):
            try:
                value = value.replace("'", '"')
                value = json.loads(value)
            except ValueError:
                self.fail('Json не валидный')

        if value and isinstance(value, list):
            try:
                return Polygon(value)
            except (GEOSException, ValueError):
                self.fail('Неправильный формат данных для созданиия полигона')
        self.fail('Неправильный формат данных для созданиия полигона')

    def to_representation(self, value):
        """
        Transform Polygon object to json.
        """
        if value is None:
            return value

        if isinstance(value, Polygon):
            value = value.coords[0]

        return value
