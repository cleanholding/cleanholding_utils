import time


class RamCache:
    expiration = 30
    value = None
    timestamp = None

    @classmethod
    def get_uncached(cls):
        raise NotImplementedError('This method has to be overloaded')

    @classmethod
    def get(cls):
        now = time.time()
        if not cls.timestamp or now - cls.timestamp > cls.expiration:
            cls.value = cls.get_uncached()
            cls.timestamp = now
        return cls.value

    @classmethod
    def expire(cls):
        cls.value = cls.get_uncached()
