import concurrent.futures
import sys
import logging

_logger = logging.getLogger(__name__)
_TESTING = sys.argv[1:2] == ['test']
_TIMEOUT = 5


def _on_complete(future):
    try:
        future.result()
    except Exception:
        _logger.error('Error in async worker', exc_info=True)


class AsyncWorker:
    def __init__(self):
        super().__init__()
        self.worker = concurrent.futures.ThreadPoolExecutor(
                max_workers=1)

    def submit(self, func, *args, **kwargs):
        future = self.worker.submit(func, *args, **kwargs)
        future.add_done_callback(_on_complete)
        if _TESTING:
            future.result(_TIMEOUT)
        return future


async_worker = AsyncWorker()
