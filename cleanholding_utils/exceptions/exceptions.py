from rest_framework.exceptions import APIException


class RestException(APIException):
    def __init__(self, detail, code=400):
        super().__init__(detail)
        self.status_code = code
