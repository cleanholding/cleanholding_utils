import json_log_formatter
import logging
import datetime
import logging

_logger = logging.getLogger(__name__)


class JSONFormatter(json_log_formatter.JSONFormatter):
    def json_record(self, message: str, extra: dict, record: logging.LogRecord):
        context = extra
        django = {
            # 'app': settings.APP_ID,
            'name': record.name,
            'filename': record.filename,
            'funcName': record.funcName,
        }
        if record.exc_info:
            django['exc_info'] = self.formatException(record.exc_info)

        return {
            'message': message,
            'timestamp': datetime.datetime.now(),
            'level': record.levelname,
            'context': context,
            'django': django
        }

    def to_json(self, record):
        try:
            return self.json_lib.dumps(
                record, ensure_ascii=False, default=str)
        # ujson doesn't support default argument and raises TypeError.
        except TypeError:
            _logger.error('Error in log formatter', exc_info=True)
            try:
                return self.json_lib.dumps(record, ensure_ascii=False)
            except TypeError:
                return '{}'
