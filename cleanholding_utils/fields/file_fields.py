import io
import re
import uuid

import pyheif
from PIL import Image
from django.contrib.gis.db import models
from django.db.models import FileField
from django.core.files import File


class ResizeImageField(models.ImageField):
    """
    To allow .heic files in Django Admin use
    `heif-image-plugin` in admin.py file
    """

    def __init__(self, *args, **kwargs):
        self.width = kwargs.pop('width', 1000)
        self.height = kwargs.pop('height', 1000)
        super().__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        uploaded_image = super(FileField, self).pre_save(model_instance, add)
        if not uploaded_image or uploaded_image._committed:
            return uploaded_image
        ext = uploaded_image.name.split('.')[-1]

        with io.BytesIO() as image_io:
            if ext in ['jpeg', 'jpg']:
                quality = 20
                image = Image.open(uploaded_image)
                image.save(image_io, save=False, quality=quality, format='JPEG')
            elif ext == 'heic':
                heif = pyheif.read_heif(uploaded_image.read())
                image = Image.frombytes(heif.mode, heif.size, heif.data,
                                        'raw', heif.mode, heif.stride)
                image.save(image_io, save=False, format='JPEG')
                ext = 'jpeg'
            else:
                image_io.write(uploaded_image.read())
            uploaded_image.save(f'{uuid.uuid4().hex}.{ext}', image_io, save=False)

        return uploaded_image


class DocumentField(models.FileField):
    def pre_save(self, model_instance, add):
        file = super(FileField, self).pre_save(model_instance, add)
        if file and not file._committed:
            # Commit the file to storage prior to saving the model
            # todo save name
            m = re.match('.*(\.[a-zA-Z0-9]*)$', file.name)
            ext = m.groups()[0] if m else ''
            file.save(uuid.uuid4().hex + ext, file.file, save=False)
        return file
