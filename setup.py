from setuptools import setup, find_packages

setup(name='cleanholding_utils',
      version='0.0.80',
      description='(Part-) self-written libraries',
      url='https://gitlab.com/cleanholding/cleanholding_utils',
      author='Evgeny Rubanenko <erubanenko@gmail.com>, Alexey Khatskevich <lesaha.95@mail.ru>',
      license='MIT License',
      install_requires=[
          'Django',
          'djangorestframework',
          'json_log_formatter',
          'heif-image-plugin',
          ],
      extras_require={
        'export': ['gspread', 'xlsxwriter']
      },
      packages=find_packages(exclude=[]),
      include_package_data=True,
      zip_safe=False)
