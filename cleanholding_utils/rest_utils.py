from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from .django_auto_prefetching import AutoPrefetchViewSetMixin


class WrapMethodsMetaclass(type):
    def __init__(cls, what, bases=None, dict=None):
        super().__init__(what, bases, dict)
        for method_name in ['post', 'get', 'put', 'patch', 'delete']:
            method = getattr(cls, method_name, None)
            if not method or not hasattr(cls, 'request_serializer'):
                continue

            def scope_crutch():
                method1 = method

                def validate_wrapper(self, request, *args):
                    serializer = self.request_serializer(data=request.data)
                    if not serializer.is_valid():
                        return Response(serializer.errors,
                                        status=status.HTTP_400_BAD_REQUEST)
                    return method1(self, request, serializer.validated_data)

                try:
                    from drf_spectacular.utils import extend_schema
                    setattr(
                        cls, method_name,
                        extend_schema(request=cls.request_serializer)(validate_wrapper))
                except ImportError:
                    setattr(
                        cls, method_name, validate_wrapper)

            scope_crutch()


class ExtApiView(APIView, metaclass=WrapMethodsMetaclass):
    def get_serializer(self):
        serializer = getattr(self, 'request_serializer', None)
        # Crutch for Swagger and OpenAPI
        if serializer:
            return serializer()
        return None


class ExtGenericViewSet(AutoPrefetchViewSetMixin, GenericViewSet):
    pass
