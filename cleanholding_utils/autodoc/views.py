from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import JSONField
from django.http import HttpResponse
from django.views.generic import TemplateView

from importlib import import_module

from .utils import get_autodoc_models, get_periodic_tasks

class AutoDocView(LoginRequiredMixin, TemplateView):
    template_name = 'autodoc/index.html'

    def get(self, request, **kwargs):
        if not request.user.is_superuser:
            return HttpResponse('The user is not superuser')

        apps_info = {}
        info = {'apps': apps_info, 'periodic_tasks': []}

        for model in get_autodoc_models():
            if model._meta.app_label not in info['apps']:
                info['apps'][model._meta.app_label] = []

            fields_info = []
            model_info = {
                'name': model.__name__,
                'verbose_name': model._meta.verbose_name,
                'fields': fields_info,
                'label': model._meta.label,
                'doc': model.__doc__,
                'db_table': model._meta.db_table,
            }
            apps_info[model._meta.app_label].append(model_info)

            for field in model._meta.get_fields():
                if str(type(field).__name__) == "ManyToOneRel":
                    continue
                field_info = {
                    'field_name': field.name,
                    'verbose_name': getattr(field, 'verbose_name', ''),
                    'help_text': getattr(field, 'help_text', ''),
                    'choices': getattr(field, 'choices', ''),
                }

                if str(type(field).__name__) in ("ManyToManyRel", "ForeignKey", "ManyToManyField",
                                                 "ManyToOneRel", "OneToOneField"):
                    field_info['type'] = f'{type(field).__name__}({field.related_model.__name__})'
                    field_info['fk_label'] = field.related_model._meta.label
                else:
                    field_info['type'] = type(field).__name__

                if isinstance(field, JSONField) and hasattr(field, 'schema'):
                    field_info['json_schema'] = {
                        'schema': dict(field.schema),
                    }

                fields_info.append(field_info)

        for pt in get_periodic_tasks():
            try:
                path, task_name = pt.task.rsplit('.', 1)
                task = getattr(import_module(path), task_name)
            except (ImportError, AttributeError):
                continue

            info['periodic_tasks'].append({
                'name': pt.name,
                'last_run_at': pt.last_run_at,
                'help_text': (task.__doc__ or '').strip(),
                'enabled': pt.enabled,
                'schedule': '{} {} {} {} {}'.format(
                    pt.crontab.minute, pt.crontab.hour,
                    pt.crontab.day_of_month, pt.crontab.month_of_year,
                    pt.crontab.day_of_week,
                ),
            })

        return self.render_to_response(context=info)
