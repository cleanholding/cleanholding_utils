from django.contrib.auth.models import User
from django.db.models import Q
from django.test import TestCase
from django.utils import timezone

from cleanholding_utils.functions.object_matching import check_match


"""
To run those tests the easiest is to link or copy this directory into
your django project
"""

class ValidateSingleObjectByQExpressionTestCase(TestCase):

    def test_q_expression(self):
        username = '79653179999'
        first_name = 'Test first_name'
        last_name = 'Test last_name'
        email = 'test123@test231.com'
        user = User(
            first_name=first_name,
            last_name=last_name,
            username=username,
            email=email,
            is_staff=False,
            is_active=True,
            date_joined=timezone.now()
        )
        expression = Q(username=username, first_name=first_name) | Q(email=email)
        assert check_match(user, expression)

        expression = Q(username=username, first_name=first_name) & ~Q(email=email)
        assert not check_match(user, expression)

        now = timezone.now()
        expression = (
            (Q(username=username, first_name=first_name) | Q(email=email)) &
            Q(date_joined__gte=now)
        )
        assert user.date_joined < now
        assert not check_match(user, expression)

        expression = ~Q(username=username, first_name='not equal first name')
        assert check_match(user, expression)

        expression = ~Q(username=username, first_name='not equal first name') & Q(last_name=last_name)
        assert check_match(user, expression)

        expression = ~Q(username=username, first_name='not equal first name') | Q(email=email)
        assert check_match(user, expression)

        expression = ~Q(username=username, first_name='not equal first name') & ~Q(last_name=last_name)
        assert not check_match(user, expression)

        expression = (
            (Q(username=username, first_name=first_name) | ~Q(email='another_email')) &
            ~Q(date_joined__gte=now)
        )
        assert check_match(user, expression)

        expression = (
            ((Q(username=username, last_name='') | Q(is_active=True)) & Q(last_name__isnull=True)) |
            (Q(date_joined__gte=now) & Q(email=email)) | Q(date_joined__lte=now, is_staff=False)
        )
        assert check_match(user, expression)

        expression = (
            ((Q(username=username, last_name='') | ~Q(is_active=True)) & Q(last_name__isnull=True)) |
            (Q(date_joined__gte=now) & Q(is_active=True)) | ~Q(date_joined__lte=now, is_staff=False)
        )
        assert not check_match(user, expression)
