class PostSaveCallbacksMixin:
    """
    Usage:
    add callbacks with attrs to self._post_save_callbacks
    they will be called after save() method

    Example:
    self.add_post_save_callback(callback, attr1=some_value, attr2=some_value)
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._post_save_callbacks = list()

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        for callback, attrs in self._post_save_callbacks:
            callback(**attrs)
        self._post_save_callbacks.clear()

    def add_post_save_callback(self, callback, **kwargs):
        self._post_save_callbacks.append(
            (callback, kwargs)
        )
