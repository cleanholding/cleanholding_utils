# Installation

1. Install `cleanholding_utils`
2. Add `cleanholding_utils` to `INSTALLED_APPS` in `config/settings.py`
3. Add `path('model_docs/', include('cleanholding_utils.autodoc.urls'))` to `urlpatterns` in `config/urls.py` 

# Settings guide

- `AUTODOC_APPS` - list of apps to parse models from, similar to `INSTALLED_APPS`
