from django.apps import apps, AppConfig
from django.conf import settings

AUTODOC_APPS_PRIORITY = ('AUTODOC_APPS', 'LOCAL_APPS', 'INSTALLED_APPS')


def get_autodoc_apps_list():
    for app_list_setting in AUTODOC_APPS_PRIORITY:
        app_list = getattr(settings, app_list_setting, None)
        if app_list is not None:
            return app_list

    raise ValueError("Can't find apps to parse with autodoc")


def get_autodoc_models():
    models = []

    for app in get_autodoc_apps_list():
        app_config = AppConfig.create(app)
        app_config.apps = apps
        app_config.import_models()
        models += list(app_config.get_models())

    return models

def get_periodic_tasks():
    try:
        from django_celery_beat.models import PeriodicTask
        return PeriodicTask.objects.all()
    except ImportError:
        return []
