import json

from django.forms.widgets import Textarea


class JSONEditor(Textarea):
    def render(self, name, value, attrs=None, **kwargs):
        json_data = json.loads(value)
        value = json.dumps(json_data, indent=2, ensure_ascii=False)
        return super().render(name, value, attrs)
