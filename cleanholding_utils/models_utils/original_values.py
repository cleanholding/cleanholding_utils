from copy import deepcopy

from django.db import models


class DirtyFieldsQueryset(models.QuerySet):
    def only(self, *fields):
        fields_updated = deepcopy(fields)
        fields_for_save = getattr(self.model, 'fields_for_save')
        fields_for_save_no_id = []
        for f in fields_for_save:
            if f.endswith('_id'):
                fields_for_save_no_id.append(f[:-3])
            else:
                fields_for_save_no_id.append(f)
        if fields_for_save:
            fields_updated += fields_for_save
        return super().only(*fields_updated)

    def default_only(self, *fields):
        return super().only(*fields)

    def defer(self, *fields):
        fields_updated = deepcopy(fields)
        fields_for_save = getattr(self.model, 'fields_for_save')
        fields_for_save_no_id = []
        for f in fields_for_save:
            if f.endswith('_id'):
                fields_for_save_no_id.append(f[:-3])
            else:
                fields_for_save_no_id.append(f)
        if fields_for_save:
            fields_updated += fields_for_save
        return super().only(*fields_updated)


class DirtyFieldsManager(models.manager.BaseManager.from_queryset(DirtyFieldsQueryset),
                         models.Manager):
    pass


class SaveOriginalValuesMixin(models.Model):

    prefix = '_original_'
    fields_for_save: tuple | list

    def _get_fields(self):
        try:
            return self.fields_for_save
        except AttributeError:
            raise AttributeError('Probably you forgot to set "fields_for_save" or you '
                                 'dont need to inherit this mixin')

    def save_previous_values(self):
        fields_for_save = self._get_fields()
        for field in fields_for_save:
            # deepcopy is being used because of mutable types (like JsonField dict)
            value = self.__dict__.get(field)
            if type(value) in [list, dict]:
                value = deepcopy(value)
            setattr(self, self.prefix + field, value)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.save_previous_values()

    def get_changes(self, auto=False, exclude_auto_fields=list()):
        changes = {}
        fields = getattr(self, 'fields_for_save', [])

        for field in fields:
            new = getattr(self, field, None)
            old = getattr(self, '_original_' + field, None)

            if new == old:
                continue

            if type(new) in [list, dict]:
                changes[field] = {
                    'old': old,
                    'new': new,
                }
            else:
                changes[field] = {
                    'old': str(old),
                    'new': str(new),
                }
            if auto and field not in exclude_auto_fields:
                changes[field]['auto'] = True
        return changes

    def any_field_changed(self, *fields, check_new=False):
        if check_new and self.pk is None:
            return True
        for field in fields:
            if getattr(self, field) != getattr(self, f"_original_{field}"):
                return True
        return False

    objects = DirtyFieldsManager()

    class Meta:
        abstract = True
        base_manager_name = 'objects'
