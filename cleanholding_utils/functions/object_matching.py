import operator
from datetime import date, datetime

from django.db.models import Q


def _in_iterable(value, container):
    return value in container


def _make_equal_dates_type(estimated_date_1, estimated_date_2):
    if isinstance(estimated_date_1, date) and isinstance(estimated_date_2, datetime):
        estimated_date_1 = datetime.combine(
            estimated_date_1,
            datetime.min.time(),
            tzinfo=estimated_date_2.tzinfo
        )
    elif isinstance(estimated_date_1, datetime) and isinstance(estimated_date_2, date):
        estimated_date_2 = datetime.combine(
            estimated_date_2,
            datetime.min.time(),
            tzinfo=estimated_date_1.tzinfo
        )
    return estimated_date_1, estimated_date_2


_OPERATORS_MAP = {
    'gte': operator.ge,
    'lte': operator.le,
    'gt': operator.gt,
    'lt': operator.lt,
    'in': _in_iterable,
    'contains': operator.contains,
    'exact': operator.eq,
    'isnull': operator.eq,
}


def _check_condition(obj, condition: tuple) -> bool:
    """
    Check single Q expression on object
    """
    query_operator = 'exact'
    expression, value_to_compare = condition
    expression = expression.split('__')
    expression_len = len(expression)
    if expression_len > 2 or expression_len == 0:
        raise NotImplementedError('You cannot use matching by related objects')
    elif expression_len == 2:
        # have some operator
        field_name, query_operator = expression
    else:
        # equal
        field_name = expression[0]
    obj_value = getattr(obj, field_name)
    if obj_value is None:
        return ((query_operator == 'isnull' and value_to_compare)
                  or (query_operator == 'exact' and value_to_compare is None))
    operator_func = _OPERATORS_MAP.get(query_operator)
    if operator_func is None:
        raise NotImplementedError(f'Operator {query_operator} was not implemented')
    obj_value, value_to_compare = _make_equal_dates_type(obj_value, value_to_compare)
    return operator_func(obj_value, value_to_compare)


def check_match(obj, query: Q) -> bool:
    connector = query.connector
    is_reversed = query.negated
    for condition in query.children:
        if isinstance(condition, Q):
            result = check_match(obj, condition)
        else:
            result = _check_condition(obj, condition)
        if result and connector == 'OR':
            return not is_reversed
        if not result and connector == 'AND':
            return is_reversed
    return connector == 'AND' and not is_reversed
