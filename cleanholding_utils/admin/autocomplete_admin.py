from django.contrib.gis import admin
from django.db.models.fields.related import RelatedField


def _is_relation(field):
    return isinstance(field, RelatedField)


class AutoCompleteMixin(object):
    def get_autocomplete_fields(self, request):
        model_fields = self.model._meta.get_fields()
        autocomplete_fields = [f.name for f in model_fields if _is_relation(f)]
        return autocomplete_fields


class ACModelAdmin(AutoCompleteMixin, admin.ModelAdmin):
    pass


class PopupFkPickerMixin(object):
    @property
    def raw_id_fields(self):
        model_fields = self.model._meta.get_fields()
        autocomplete_fields = [f.name for f in model_fields if _is_relation(f)]
        return autocomplete_fields


class PFPModelAdmin(PopupFkPickerMixin, admin.ModelAdmin):
    pass
