# Utils for cleanholding projects

(Part-) self-written libraries / helpers, that are used in cleanholding projects (kindschool, ubirator, etc.)

To use export to excel/google docs from [cleanholding_utils/export_serializer.py](https://gitlab.com/cleanholding/cleanholding_utils/-/blob/master/cleanholding_utils/export_serializer.py) please install package via `pip install git+https://gitlab.com/cleanholding/cleanholding_utils.git#egg=cleanholding_utils[export]`
