import re
from string import punctuation

from django.core.exceptions import ValidationError


def clean(phone):
    if phone == '' or phone is None:
        return None

    # Cleaning
    regex = re.compile('[%s]' % re.escape(punctuation + ' '))
    phone = regex.sub('', phone)

    # Asserts
    if phone[0] not in ['7', '8']:
        raise ValidationError('Телефонный номер должен начинаться с 7 или 8')
    if not re.match('^\d{11}$', phone):
        raise ValidationError('Телефонный номер должен состоять из 11 цифр')

    phone = '7' + phone[1:]
    return phone
