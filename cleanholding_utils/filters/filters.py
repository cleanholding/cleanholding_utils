import django_filters
from django import forms
from django.core.validators import EMPTY_VALUES


class HasKeyFilter(django_filters.Filter):
    field_class = forms.CharField

    def __init__(self, *args, **kwargs):
        self.null_value = ''
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if value != self.null_value:
            return super().filter(qs, value)

        qs = self.get_method(qs)(**{'%s__%s' % (self.field_name, self.lookup_expr): None})
        return qs.distinct() if self.distinct else qs


class ContainsIntFilter(django_filters.BaseInFilter):
    field_class = forms.IntegerField

    def __init__(self, *args, **kwargs):
        self.null_value = ''
        super().__init__(*args, **kwargs)

    def filter(self, qs, value):
        if value != self.null_value:
            return super().filter(qs, value)

        qs = self.get_method(qs)(**{'%s__%s' % (self.field_name, self.lookup_expr): None})
        return qs.distinct() if self.distinct else qs


class EmptyStringFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is False)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.field_name: ""})


class ExcludeFilter(django_filters.Filter):
    """
    Документация rest_framework_filters предлагает
    использовать синтаксис != в фильтре для exclude
    но с связанными обьектами она работает не корректно,
    т.к. в результате используется INNER JOIN для связанных объектов,
    из-за этого объекты, на которые не ссылаются другие объекты
    не попадают в результаты, этот фильтр решает эту проблему
    """
    field_class = forms.CharField

    def __init__(self, field_name, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.field_name = field_name

    def filter(self, qs, value):
        return qs.exclude(**{self.field_name: value})


class EmptyListFilter(django_filters.BooleanFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs

        exclude = self.exclude ^ (value is False)
        method = qs.exclude if exclude else qs.filter

        return method(**{self.field_name: []})
