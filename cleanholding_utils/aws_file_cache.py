import contextlib
import locale
import logging
import os
import time

import requests

logger = logging.getLogger(__name__)


# Crutch for if-modified-since (to not write it in russian).
@contextlib.contextmanager
def setlocale(*args, **kw):
    saved = locale.setlocale(locale.LC_ALL)
    yield locale.setlocale(*args, **kw)
    locale.setlocale(locale.LC_ALL, saved)


class Uploader(object):
    def __init__(self, base_url, base_path):
        self.base_url = base_url
        self.base_path = base_path

    def upload_file(self, file_name):
        file_path = str(self.base_path.path(file_name))
        logger.info('Path to file to upload', file_path)
        file_url = self.base_url + file_name
        headers = {}
        try:
            stat = os.stat(file_path)
            if stat.st_size > 0:
                with setlocale(locale.LC_TIME, 'en_US.UTF-8'):
                    mod_time = time.strftime('%a, %d %b %Y %H:%M:%S GMT', time.gmtime(stat.st_ctime))
                headers['If-Modified-Since'] = mod_time
        except FileNotFoundError:
            pass
        request = requests.get(file_url, headers=headers)
        if request.status_code == 200:
            logger.info('Downloading')
            with open(file_path, 'wb') as fd:
                fd.write(request.content)
        logger.info('Up to date')
