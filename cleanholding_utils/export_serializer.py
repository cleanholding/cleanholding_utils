import io
import json
import time
import types
from abc import ABC, abstractmethod
from datetime import date, datetime
from decimal import Decimal
from itertools import chain, zip_longest

import pytz
from django.conf import settings
from django.db.models import DateField, DateTimeField
from django.db.models.fields.related import ForeignKey
from django.db.models.fields.reverse_related import ManyToManyRel, ManyToOneRel

GOOGLE_EXPORT_PARAMS = {'valueInputOption': 'RAW'}
TZ = getattr(settings, 'TIME_ZONE', 'UTC')
TZ = pytz.timezone(TZ)

TIME_ROW = 'Время обновления: {now}, продолжительность выгрузки: {duration}'

GOOGLE_FIRST_DAY = date(1899, 12, 30)
GOOGLE_FIRST_DT = datetime.combine(
    GOOGLE_FIRST_DAY, datetime.min.time()).replace(tzinfo=settings.TZ)


class FieldForExport:
    def __init__(self, *args, **kwargs):
        for name, value in kwargs.items():
            setattr(self, name, value)

    def export(self, serializer, obj, row):
        source = getattr(self, 'source', None)
        if source:
            if source in serializer.__class__.__dict__:
                row.append(getattr(serializer, source)(obj))
                return
            obj_attr = getattr(obj, source)
            if obj_attr:
                row.append(obj_attr)


class Sheet:
    orientation = 'rows'

    def set_fields_from_serializer(self):
        self.field_map = {
            field['field_name']: ix
            for ix, field in enumerate(self.serializer.get_header_description())
        }
        self.field_formats = {
            field['field_name']: field['format']
            for field in self.serializer.get_header_description() if field.get('format')
        }
        self.header = [field['name'] for field in self.serializer.get_header_description()]

    @classmethod
    def fabricate_from_serializer_exporter(cls, serializer_exporter):
        self = cls()
        self.data = []
        self.sheet_name = serializer_exporter.sheet_name
        self.queryset = serializer_exporter.export_serializer_cfg['queryset']
        self.serializer = serializer_exporter.export_serializer_cfg['serializer']
        self.set_fields_from_serializer()
        self.data = ExportSerializer.get_data_to_list(
            self.serializer,
            self.queryset
        )
        return self

    @classmethod
    def fabricate_from_serializer(cls, serializer, sheet_name):
        self = cls()
        self.data = []
        self.serializer = serializer
        self.sheet_name = sheet_name
        self.set_fields_from_serializer()
        self.data.append(self.header)
        return self

    @classmethod
    def fabricate_from_function(cls, sheet_name):
        self = cls()
        self.data = []
        self.sheet_name = sheet_name
        header_attrs = [(fname, value)
                        for fname, value in cls.__dict__.items() if fname.isupper()]
        self.sheet_name = sheet_name
        self.field_formats = {value[0]: value[1]
                              for key, value in header_attrs if len(value) == 2}
        self.header = [value[0] for fname, value in header_attrs]
        self.field_map = {
            fname: ix
            for ix, fname in enumerate(self.header)
        }
        [setattr(self, fname, val[0]) for fname, val in header_attrs]
        self.data.append(self.header)
        return self

    def add_row(self, dict_row):
        list_row = [dict_row.get(key) for key in self.header]
        self.data.append(list_row)

    @staticmethod
    def col_to_num(col_str):
        """ Convert base26 column string to number. """
        expn = 0
        col_num = 0
        for char in reversed(col_str):
            col_num += (ord(char) - ord('A') + 1) * (26 ** expn)
            expn += 1

        return col_num

    def columns_to_rows(self):
        columns_data = [
            (col[0], self.col_to_num(col[1])) for col in self.data
        ]
        column_nums = [column_group[1] for column_group in columns_data]
        column_data = [column_group[0] for column_group in columns_data]

        max_col = max(column_nums)
        len_last_column_data = len(column_data[0][-1])
        max_col += len_last_column_data - 1

        rows = []
        for zipped_data in zip_longest(*column_data, fillvalue=''):
            row = ['' for i in range(max_col)]
            for i, zipped_data_item in enumerate(zipped_data):
                col_num = column_nums[i] - 1
                for q, item in enumerate(zipped_data_item):
                    row[col_num + q] = item
            rows.append(row)
        self.data = rows


class Filler(ABC):
    def __init__(self, table):
        self.table = table
        self.sheet = None
        self.field_map = None
        self.field_formats = None
        self.header = None
        self.data = None

    def build_sheet_data(self, sheet_data_obj):
        self.sheet = self.get_sheet(sheet_data_obj.sheet_name)
        self.sheet_name = sheet_data_obj.sheet_name
        self.field_map = getattr(sheet_data_obj, 'field_map', {})
        self.field_formats = getattr(sheet_data_obj, 'field_formats', {})
        self.header = getattr(sheet_data_obj, 'header', {})
        self.data = sheet_data_obj.data
        self.set_field_formats()

    @abstractmethod
    def set_field_formats(self):
        raise NotImplementedError()

    @abstractmethod
    def get_sheet(self, sheet_name):
        raise NotImplementedError()

    @abstractmethod
    def write_sheet(self):
        raise NotImplementedError()


class ExcelFiller(Filler):
    def write_sheet(self, skip_first_line_format=True):
        self.format_objs = {}
        for fformat in self.date_formats.keys():
            date_format = self.table.add_format({'num_format': fformat,
                                               'align': 'left'})
            self.format_objs[fformat] = date_format

        not_datetime_columns = list(range(len(self.data[0])))
        date_columns = sorted(list(chain.from_iterable(self.date_formats.values())), reverse=True)
        for col in date_columns:
            not_datetime_columns.pop(col)

        if skip_first_line_format:
            self.sheet.write_row(0, 0, self.data[0])
            line_start = 1
        else:
            line_start = 0

        for row, line in enumerate(self.data[line_start:]):
            corr_row = row + 1
            line_tuple = tuple(line)
            for fformat, columns in self.date_formats.items():
                for col in columns:
                    date_col = line_tuple[col]
                    if isinstance(date_col, datetime):
                        date_col = date_col.astimezone(pytz.timezone('Etc/GMT-3')).replace(
                            tzinfo=None)
                    if date_col:
                        self.sheet.write_datetime(
                            corr_row,
                            col,
                            date_col,
                            self.format_objs[fformat]
                        )
            for col in not_datetime_columns:
                self.sheet.write(corr_row, col, line_tuple[col])

    def write_rows(self, row, data):
        col = 0
        for ix, point in enumerate(data):
            self.sheet.write_row(row + ix, col, point)

    def set_field_formats(self):
        self.date_formats = {}
        for fname, fformat in self.field_formats.items():
            if fformat not in self.date_formats:
                self.date_formats[fformat] = []
            self.date_formats[fformat].append(self.field_map[fname])

    def get_sheet(self, sheet_name):
        worksheet = self.table.add_worksheet(sheet_name)
        return worksheet


class GoogleDocsFiller(Filler):
    def time_to_google(self, time):
        if time:
            if isinstance(time, datetime):
                diff = time - GOOGLE_FIRST_DT
            else:
                diff = time - GOOGLE_FIRST_DAY
            return diff.days + diff.seconds / (24 * 60 * 60)
        return time

    def decimal_to_float(self, num):
        from gspread.utils import numericise
        return numericise(num if num is None else str(num))

    def write_sheet(self, skip_first_line_format=True):
        self.sheet.clear()
        self.write_rows()
        if self.date_formats:
            self.apply_date_formats(skip_first_line_format)

    def convert_cells_data_to_google_allowed_formats(self):
        for point in self.data[1:]:
            for field in self.date_fields:
                point[field] = self.time_to_google(point[field])
            for field in self.decimal_fields:
                point[field] = self.decimal_to_float(point[field])

    def write_rows(self, row=None, data=None):
        if not data:
            data = self.data
            self.convert_cells_data_to_google_allowed_formats()
        body = {'values': data}
        self.sheet.spreadsheet.values_append(self.sheet.title, GOOGLE_EXPORT_PARAMS, body)

    def set_field_formats(self):
        self.date_formats = []
        self.date_fields = []
        self.decimal_fields = []
        for fname, fformat in self.field_formats.items():
            if fname not in self.field_map:
                continue
            if fformat == 'float':
                self.decimal_fields.append(self.field_map[fname])
                continue
            self.date_formats.append({
                'col': self.field_map[fname],
                'rows': len(self.data),
                'format': fformat
            })
            self.date_fields.append(self.field_map[fname])

    def apply_date_formats(self, skip_first_line_format=1):
        update_data = []
        if skip_first_line_format:
            line_start = 1
        else:
            line_start = 0
        for fmt in self.date_formats:
            update_data.append({
                "repeatCell": {
                    "range": {
                        "sheetId": self.sheet.id,
                        "startRowIndex": line_start,
                        "endRowIndex": fmt['rows'] + 1,
                        "startColumnIndex": fmt['col'],
                        "endColumnIndex": fmt['col'] + 1,
                    },
                    "cell": {
                        "userEnteredFormat": {
                            "numberFormat": {
                                "type": "DATE",
                                "pattern": fmt['format'],
                            }
                        }
                    },
                    "fields": "userEnteredFormat.numberFormat"
                }
            })
        if update_data:
            self.sheet.spreadsheet.batch_update({"requests": update_data})

    def get_sheet(self, sheet_name):
        import gspread
        try:
            sheet = self.table.worksheet(sheet_name)
        except gspread.exceptions.WorksheetNotFound:
            sheet = self.table.add_worksheet(sheet_name, rows=0, cols=0)
        return sheet


class JsonFiller(Filler):
    def get_sheet(self, sheet_name):
        dct = {}
        self.table.append(dct)
        return dct

    def set_field_formats(self):
        self.header_extended = []
        for fname, ix in self.field_map.items():
            name = self.header[ix]
            field_desc = {
                'name': name,
            }
            if fname in self.field_formats:
                field_desc['format'] = self.field_formats[fname]

            self.header_extended.append(field_desc)

    def write_sheet(self, skip_first_line_format=True):
        self.write_rows()

    def write_rows(self, row=None, data=None):
        if not data:
            data = self.data

        self.sheet['header'] = self.header_extended
        self.sheet['name'] = self.sheet_name
        self.sheet['data'] = self.data[1:]
        self.sheet['type'] = 'table'


class Exporter:
    # TODO: Fillers should be removed and passed to `export` instead
    def __init__(self, fillers=None, extra_kwargs=None):
        self.fillers = fillers
        if not extra_kwargs:
            extra_kwargs = {}
        self.extra_kwargs = extra_kwargs

    def export(self, fillers=None):
        if fillers:
            self.fillers = fillers
        assert self.fillers is not None
        self.export_function(**self.extra_kwargs)

    def write(self, sheet):
        for filler in self.fillers:
            filler.build_sheet_data(sheet)
            filler.write_sheet()

    def export_json(self, jsn=None):
        if not jsn:
            jsn = []
        self.fillers = [JsonFiller(jsn)]
        self.export_function(**self.extra_kwargs)
        return jsn

    def export_excel(self, bytes_io=None):
        import xlsxwriter
        if not bytes_io:
            bytes_io = io.BytesIO()
        table = xlsxwriter.Workbook(bytes_io)
        self.fillers = [ExcelFiller(table)]
        self.export_function(**self.extra_kwargs)
        table.close()
        return bytes_io


class SerializerExporter(Exporter):

    def export_function(self):
        sheet = Sheet.fabricate_from_serializer_exporter(self)
        self.write(sheet)


class ExportSerializer:
    def __init__(self):
        self.concrete_fields = {}
        self.choices = {}
        for fld in self.Meta.model._meta.concrete_fields:
            self.concrete_fields[fld.name] = fld
            if hasattr(fld, 'choices') and fld.choices:
                choices = {}
                for k, v in fld.choices:
                    choices[k] = v
                self.choices[fld.name] = choices
        if getattr(self.Meta, 'fields', None):
            fields = self.Meta.fields
            if fields == '__all__':
                fields = list(self.concrete_fields.keys())
        elif getattr(self.Meta, 'exclude', None):
            exclude = self.Meta.exclude
            fields = [
                field for field in self.concrete_fields.keys()
                if field not in exclude]
        else:
            raise Exception('Fields meta attribute is required')
        self.fields = fields

        return super().__init__()

    def export(self, obj, row=None):
        if row is None:
            row = []
        if obj is None:
            row += [None] * len(self.get_header_description())
            return row
        fields = self.fields
        for field in fields:
            exporter = getattr(self, field, None)
            if callable(exporter):
                row.append(exporter(obj))
                continue
            if exporter and isinstance(exporter, ExportSerializer):
                exporter.export(getattr(obj, field), row)
                continue

            if isinstance(exporter, FieldForExport):
                source = getattr(exporter, 'source', None)
                if source:
                    cls_field = self.concrete_fields.get(field)
            else:
                cls_field = self.concrete_fields.get(field)

            if exporter and (isinstance(exporter, FieldForExport) and not cls_field):
                exporter.export(self, obj, row)
                continue

            if not cls_field:
                raise Exception(f'field {field} is not described')
            value = getattr(obj, field, None)
            if value is None:
                pass
            elif field in self.choices:
                value = self.choices[field][value]
            elif isinstance(value, (str, float, int, date)) :
                pass
            elif isinstance(value, Decimal):
                value = float(value)
            elif isinstance(value, (list, dict)):
                value = json.dumps(value)
            elif isinstance(value, datetime):
                local_dt = value.astimezone(TZ)
                value = local_dt
            else:
                value = str(value)
            row.append(value)
        return row

    def get_header(self):
        return [field['name'] for field in self.get_header_description()]

    def get_header_description(self):
        header = []
        fields = self.fields
        for field in fields:
            exporter = getattr(self, field, None)
            if isinstance(exporter, FieldForExport):
                source = getattr(exporter, 'source', None)
                if source:
                    cls_field = self.concrete_fields.get(field)

            cls_field = self.concrete_fields.get(field)
            name = field
            if cls_field:
                name = str(cls_field.verbose_name)
            if callable(exporter) or (isinstance(exporter, FieldForExport) and not cls_field):
                # TODO: add name to the function
                name = getattr(exporter, 'name', field)
                field_header = {'field_name': field, 'name': name}
                format = getattr(exporter, 'format', None)
                if format:
                    field_header['format'] = format
                header.append(field_header)
                continue
            elif exporter and isinstance(exporter, ExportSerializer):
                sub_header = exporter.get_header_description()
                for sh in sub_header:
                    sh['field_name'] = f'{field}.{sh["field_name"]}'
                    # Reverse naming to bring important part to the front
                    sh['name'] = f'{sh["name"]}({name})'
                header += sub_header
                continue
            if not cls_field:
                raise Exception(f'field {field} is not described')
            # Str is important because of a proxy objects
            field_header = {
                'field_name': field,
                'name': name
            }
            format = getattr(exporter, 'format', None)
            if format:
                field_header['format'] = format
            elif isinstance(cls_field, DateTimeField):
                field_header['format'] = 'yy.mm.dd hh:mm:ss'
            elif isinstance(cls_field, DateField):
                field_header['format'] = 'yy.mm.dd'
            header.append(field_header)
        return header

    @staticmethod
    def prefetch_queryset(serializer, queryset):
        prefetch_related_fields = set()
        select_related_fields = set()
        # TODO: make it recursive with storing fields cache
        for field in serializer.concrete_fields.values():
            if isinstance(field, ForeignKey):
                select_related_fields.add(field.name)
            elif isinstance(field, (ManyToOneRel, ManyToManyRel)):
                prefetch_related_fields.add(field.related_name)
        return queryset.select_related(
            *select_related_fields
        ).prefetch_related(*prefetch_related_fields)

    @classmethod
    def get_data_to_list(cls, serializer, queryset):
        data = [serializer.get_header()]
        for element in cls.prefetch_queryset(serializer, queryset):
            row = serializer.export(element)
            data.append(row)
        return data
